using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Mikhail
//https://stackoverflow.com/questions/43709392/camera-always-behind-player-in-unity3d


public class CameraFollowCar : MonoBehaviour
{
    public GameObject target;
    public float speed = 5;

    Vector3 offset;

    void Start()
    {
        offset = target.transform.position - transform.position;
    }

    void LateUpdate()
    {
        // Look
        var newRotation = Quaternion.LookRotation(target.transform.position - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, speed * Time.deltaTime);

        // Move
        Vector3 newPosition = target.transform.position - target.transform.forward * offset.z - target.transform.up * offset.y;
        transform.position = Vector3.Slerp(transform.position, newPosition, Time.deltaTime * speed);
    }
}