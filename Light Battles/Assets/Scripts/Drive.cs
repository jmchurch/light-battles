using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drive : MonoBehaviour
{
    public WheelCollider[] WCs;
    public GameObject[] Wheels;
    public float torque = 1000;
    public float uprightTorque = 20;
    public float maxSteerAngle = 30;
    public float Thrust = 300f;
    public float Breaking = 200f;
    public ParticleSystem boostUp;
    // ask for four different wheels, I will make it rear wheel drive
    // only steering in the front

    //making the center of mass visible in inspector
    public Vector3 com;
    public Rigidbody rb;

    // think about addRelativeForce for breaking
    // addRelativeForce for acceleration and breaking should help with the manuverability of the car (hopefully it still feels smooth)




    void Go(float accel, float steer)
    {
        accel = Mathf.Clamp(accel, -1, 1);
        steer = Mathf.Clamp(steer, -1, 1) * maxSteerAngle;
        float thrustTorque = accel * torque;
        // looop through array of wheels and wheel colliders
        for (int i = 0; i < 4; i++)
        {
            //acceleration input
            //only accelerate through back two tires
            if (i > 2)
            {
                WCs[i].motorTorque = thrustTorque;
            }
            // steering input
            //only steer the front two tires
            if (i < 2) WCs[i].steerAngle = steer;

            if (steer < 0)
            {
                // How I think this should work:
                // The rigid body should have torque put on it that counteracts the 
                // force from the turning car
                // this happens because steer is a value from -1 to 1 and if you multiply it
                // by -1 then it will be the opposite (hopefully)
                // works! but the turn radius is pretty abismal
                rb.AddRelativeTorque(Vector3.up * uprightTorque * steer * -1);
            } else if (steer > 0)
            {
                rb.AddRelativeTorque(Vector3.up * uprightTorque * steer * -1);
            }

            // Acceleration is not as peppy as I want and the breaking sucks.
            // I will add extra force to the car when the accel variable is above 0
            // and add foce in the opposite direction when the accel variable is less than 0
            if (accel > 0)
            {
                rb.AddRelativeForce(Vector3.forward * Thrust);
            } else if (accel < 0) {
                rb.AddRelativeForce(Vector3.back * Breaking);
            }

            // adding explosive force is space is pressed
            if (Input.GetButtonDown("Jump"))
            {
                rb.AddForce(transform.up * 100000);
                boostUp.Play(true);
            }

            //align the mesh with the rotation
            Quaternion quat;
            Vector3 position;
            WCs[i].GetWorldPose(out position, out quat);
            Wheels[i].transform.position = position;
            Wheels[i].transform.rotation = quat;
        }
    }

    void Update()
    {
        float a = Input.GetAxis("Vertical");
        float s = Input.GetAxis("Horizontal");
        Go(a,s);
    }
}
